/*
 * Paraphernalia - utility classes for Java programs
 * Copyright (C) 2008 Dominik Maehl 
 * 
 * Paraphernalia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Paraphernalia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Paraphernalia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dsnine.paraphernalia.chimney;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.dsnine.paraphernalia.XMLUtil;

public class Chimney {

	private final File referenceWxsFile;
	private final File generatedWxsFile;
	private final File targetWxsFile;
	private Document referenceDocument;
	private Document generatedDocument;
	private Document targetDocument;
	private XPath xPath;

	public Chimney(File referenceWxsFile, File heatGeneratedWxsFile, File targetWxsFile) {
		this.referenceWxsFile = referenceWxsFile;
		this.generatedWxsFile = heatGeneratedWxsFile;
		this.targetWxsFile = targetWxsFile;
		xPath = XPathFactory.newInstance().newXPath();
	}

	public void run() throws SAXException, IOException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException, XPathExpressionException {
		referenceDocument = XMLUtil.readDocumentFromFile(referenceWxsFile);
		generatedDocument = XMLUtil.readDocumentFromFile(generatedWxsFile);
		targetDocument = XMLUtil.createDocument();

		System.out.println("Populating target with data from reference...");
		copyReferenceToTarget();

		System.out.println("Determining basic product information...");
		gatherInformation();

		System.out.println("Deleting old fragments...");
		deleteFragmentsFromTarget();

		System.out.println("Copying new fragments...");
		copyFragmentsFromGeneratedToTarget();

		System.out.println("Analyzing differences and updating fragments...");
		updateFragments();

		System.out.println("Writing target to disk...");
		XMLUtil.writeDocumentToFile(targetDocument, targetWxsFile);
		System.out.println("Done!");
	}

	private void gatherInformation() throws XPathExpressionException, IOException {
		String oldProductId = xPath.evaluate("//Product/@Id", referenceDocument);
		String newProductId = xPath.evaluate("//Product/@Id", generatedDocument);
		String updateCode = xPath.evaluate("//Product/@UpgradeCode", referenceDocument);
		String manufacturer = xPath.evaluate("//Product/@Manufacturer", referenceDocument);
		String name = xPath.evaluate("//Product/@Name", referenceDocument);
		String version = xPath.evaluate("//Product/@Version", referenceDocument);
		System.out.println();
		System.out.println("Information:");
		System.out.println();
		System.out.println("  old Product ID: " + oldProductId);
		System.out.println("  new Product ID: " + newProductId);
		System.out.println();
		System.out.println("  Upgrade Code:   " + updateCode);
		System.out.println("  Manufacturer:   " + manufacturer);
		System.out.println("  Name:           " + name);
		System.out.println("  Version:        " + version);

		String newVersion = "";
		System.out.println();
		while (newVersion.length() == 0) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.printf("  Enter new version (x.x.x.x): ");
			newVersion = reader.readLine();
		}

		System.out.println();
		Element productElement = (Element) xPath.evaluate("//Product", targetDocument, XPathConstants.NODE);
		productElement.setAttribute("Id", newProductId);
		productElement.setAttribute("Version", newVersion);
	}

	private void copyReferenceToTarget() {
		Element documentElement = referenceDocument.getDocumentElement();
		Node importedElement = targetDocument.importNode(documentElement, true);
		targetDocument.appendChild(importedElement);
	}

	private void deleteFragmentsFromTarget() throws XPathExpressionException {
		NodeList fragmentNodes = (NodeList) xPath.evaluate("/Wix/Fragment", targetDocument, XPathConstants.NODESET);
		for (int i = 0; i < fragmentNodes.getLength(); i++) {
			Node fragmentNode = fragmentNodes.item(i);
			boolean isTargetDirDeclaration = (Boolean) xPath.evaluate("Directory[@Name='SourceDir']", fragmentNode, XPathConstants.BOOLEAN);
			boolean isTargetDirReference = (Boolean) xPath.evaluate("DirectoryRef[@Id='TARGETDIR']", fragmentNode, XPathConstants.BOOLEAN);
			boolean isInstallDirReference = (Boolean) xPath.evaluate("DirectoryRef[@Id='INSTALLDIR']", fragmentNode, XPathConstants.BOOLEAN);
			if (!isTargetDirDeclaration && !isTargetDirReference && !isInstallDirReference) {
				fragmentNode.getParentNode().removeChild(fragmentNode);
			}
		}

		NodeList componentRefNodes = (NodeList) xPath.evaluate("//Feature/ComponentRef", targetDocument, XPathConstants.NODESET);
		for (int i = 0; i < componentRefNodes.getLength(); i++) {
			Node componentRefNode = componentRefNodes.item(i);
			componentRefNode.getParentNode().removeChild(componentRefNode);
		}
	}

	private void copyFragmentsFromGeneratedToTarget() throws XPathExpressionException {
		NodeList fragmentNodes = (NodeList) xPath.evaluate("/Wix/Fragment", generatedDocument, XPathConstants.NODESET);
		for (int i = 0; i < fragmentNodes.getLength(); i++) {
			Node fragmentNode = fragmentNodes.item(i);
			boolean isTargetDirDeclaration = (Boolean) xPath.evaluate("Directory[@Name='SourceDir']", fragmentNode, XPathConstants.BOOLEAN);
			boolean isTargetDirReference = (Boolean) xPath.evaluate("DirectoryRef[@Id='TARGETDIR']", fragmentNode, XPathConstants.BOOLEAN);
			boolean isInstallDirReference = (Boolean) xPath.evaluate("DirectoryRef[@Id='INSTALLDIR']", fragmentNode, XPathConstants.BOOLEAN);
			if (!isTargetDirDeclaration && !isTargetDirReference && !isInstallDirReference) {
				fragmentNode = targetDocument.importNode(fragmentNode, true);
				Node wixNode = (Node) xPath.evaluate("/Wix", targetDocument, XPathConstants.NODE);
				wixNode.appendChild(fragmentNode);
			}
		}

		NodeList componentRefNodes = (NodeList) xPath.evaluate("//Feature/ComponentRef", generatedDocument, XPathConstants.NODESET);
		for (int i = 0; i < componentRefNodes.getLength(); i++) {
			Node componentRefNode = componentRefNodes.item(i);
			componentRefNode = targetDocument.importNode(componentRefNode, true);
			Node featureNode = (Node) xPath.evaluate("//Feature", targetDocument, XPathConstants.NODE);
			featureNode.appendChild(componentRefNode);
		}
	}

	private void updateFragments() throws XPathExpressionException {
		NodeList targetComponentNodes = (NodeList) xPath.evaluate("//Component", targetDocument, XPathConstants.NODESET);

		for (int i = 0; i < targetComponentNodes.getLength(); i++) {
			Element targetComponentNode = (Element) targetComponentNodes.item(i);
			String targetGuid = xPath.evaluate("@Guid", targetComponentNode);
			if (targetGuid.length() != 0) {
				String source = xPath.evaluate("File/@Source", targetComponentNode);
				if (source.length() != 0) {
					Node referenceFileNode = (Node) xPath.evaluate("//Component/File[@Source='" + source + "']", referenceDocument, XPathConstants.NODE);
					if (referenceFileNode != null) {
						String referenceGuid = xPath.evaluate("@Guid", referenceFileNode.getParentNode());
						targetComponentNode.setAttribute("Guid", referenceGuid);
						System.out.println("OLD: " + source);
					} else {
						System.out.println("NEW: " + source);
					}
				}
			}
		}
	}
}
