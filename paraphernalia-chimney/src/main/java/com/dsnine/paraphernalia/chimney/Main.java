/*
 * Paraphernalia - utility classes for Java programs
 * Copyright (C) 2008 Dominik Maehl 
 * 
 * Paraphernalia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Paraphernalia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Paraphernalia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dsnine.paraphernalia.chimney;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		System.out.println("Chimney " + "1.0-SNAPSHOT" + " - Copyright (C) 2008  Dominik Maehl");
		System.out.println("This program comes with ABSOLUTELY NO WARRANTY");
		System.out.println("and is free software. See README for details.");
		System.out.println("");

		if (args.length != 3) {
			System.out.println("USAGE: chimney <reference.wxs> <generated.wxs> <target.wxs>");
		}

		File referenceWxsFile = new File(args[0]);
		File heatWxsFile = new File(args[1]);
		File targetWxsFile = new File(args[2]);

		Chimney chimney = new Chimney(referenceWxsFile, heatWxsFile, targetWxsFile);

		try {
			chimney.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
