/*
 * Paraphernalia - utility classes for Java programs
 * Copyright (C) 2008 Dominik Maehl 
 * 
 * Paraphernalia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Paraphernalia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Paraphernalia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dsnine.paraphernalia;

import java.util.HashMap;

public class IDUtil {

	public static IDUtil create() {
		return new IDUtil();
	}

	private HashMap<String, Long> ids;

	private IDUtil() {
		ids = new HashMap<String, Long>();
	}

	public String newId(String discriminator) {
		if (!ids.containsKey(discriminator)) {
			ids.put(discriminator, 1L);
		}

		Long count = ids.get(discriminator);
		ids.put(discriminator, count + 1);

		return discriminator + "_" + count;
	}

}
