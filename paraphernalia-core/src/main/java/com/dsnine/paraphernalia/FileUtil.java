/*
 * Paraphernalia - utility classes for Java programs
 * Copyright (C) 2008 Dominik Maehl 
 * 
 * Paraphernalia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Paraphernalia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Paraphernalia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dsnine.paraphernalia;

import java.io.File;

public class FileUtil {

	public static boolean isChildOf(File potentialChild, File parent) {
		String parentPath = parent.getAbsolutePath();
		String childPath = potentialChild.getAbsolutePath();

		return childPath.indexOf(parentPath) == 0;
	}

	public static String getRelativePath(File file, File relativeTo) {
		if (!isChildOf(file, relativeTo)) {
			return null;
		}

		String relativePath = file.getAbsolutePath().substring(relativeTo.getAbsolutePath().length());
		if (relativePath.indexOf(System.getProperty("file.separator")) == 0 && relativePath.length() > 1) {
			return relativePath.substring(1);
		} else {
			return relativePath;
		}
	}
}
