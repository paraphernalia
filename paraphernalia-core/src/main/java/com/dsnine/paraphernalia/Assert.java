/*
 * Paraphernalia - utility classes for Java programs
 * Copyright (C) 2008 Dominik Maehl 
 * 
 * Paraphernalia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Paraphernalia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Paraphernalia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.dsnine.paraphernalia;

public class Assert {

	public static void isNotNull(Object object) {
		isNotNull(object, "object is null");
	}

	public static void isNull(Object object) {
		isNull(object, "object is not null");
	}

	public static void isTrue(boolean expression) {
		isTrue(expression, "expression is false");
	}

	public static void isFalse(boolean expression) {
		isFalse(expression, "expression is true");
	}

	public static void isNotNull(Object object, String message) {
		if (object == null) {
			throw new AssertionFailedException(message);
		}
	}

	public static void isNull(Object object, String message) {
		if (object != null) {
			throw new AssertionFailedException(message);
		}
	}

	public static void isTrue(boolean expression, String message) {
		if (!expression) {
			throw new AssertionFailedException(message);
		}
	}

	public static void isFalse(boolean expression, String message) {
		if (expression) {
			throw new AssertionFailedException(message);
		}
	}
}
